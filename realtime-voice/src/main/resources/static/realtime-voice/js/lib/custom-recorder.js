(function(window) {
	var ws;
	var Recorder = function(stream, config) {
		config = config || {};
		config.sampleBits = config.sampleBits || 8;
		config.sampleRate = config.sampleRate || (44100 / 6);
		
		var context = new(window.webkitAudioContext || window.AudioContext)();
		var audioInput = context.createMediaStreamSource(stream);
		var createScript = context.createScriptProcessor || context.createJavaScriptNode;
		var recorder = createScript.apply(context, [4096, 1, 1]);

		var audioData = {
			size: 0,
			buffer: [],
			inputSampleRate: context.sampleRate,
			inputSampleBits: 16,
			outputSampleRate: config.sampleRate,
			oututSampleBits: config.sampleBits,
			clear: function() {
				this.buffer = [];
				this.size = 0;
			},
			input: function(data) {
				this.buffer.push(new Float32Array(data));
				this.size += data.length;
			},
			compress: function() {
				var data = new Float32Array(this.size);
				var offset = 0;
				for (var i = 0; i < this.buffer.length; i++) {
					data.set(this.buffer[i], offset);
					offset += this.buffer[i].length;
				}
				var compression = parseInt(this.inputSampleRate / this.outputSampleRate);
				var length = data.length / compression;
				var result = new Float32Array(length);
				var index = 0,
					j = 0;
				while (index < length) {
					result[index] = data[j];
					j += compression;
					index++;
				}
				return result;
			},
			encodePCM: function() {
				var sampleRate = Math.min(this.inputSampleRate, this.outputSampleRate);
				var sampleBits = Math.min(this.inputSampleBits, this.oututSampleBits);
				var bytes = this.compress();
				this.buffer = [];
				this.size = 0;
				var dataLength = bytes.length * (sampleBits / 8);
				var buffer = new ArrayBuffer(44 + dataLength);
				var data = new DataView(buffer);
				var channelCount = 1;
				var offset = 0;
				var writeString = function(str) {
					for (var i = 0; i < str.length; i++) {
						data.setUint8(offset + i, str.charCodeAt(i));
					}
				}
				writeString('RIFF');
				offset += 4;
				data.setUint32(offset, 36 + dataLength, true);
				offset += 4;
				writeString('WAVE');
				offset += 4;
				writeString('fmt ');
				offset += 4;
				data.setUint32(offset, 16, true);
				offset += 4;
				data.setUint16(offset, 1, true);
				offset += 2;
				data.setUint16(offset, channelCount, true);
				offset += 2;
				data.setUint32(offset, sampleRate, true);
				offset += 4;
				data.setUint32(offset, channelCount * sampleRate * (sampleBits / 8), true);
				offset += 4;
				data.setUint16(offset, channelCount * (sampleBits / 8), true);
				offset += 2;
				data.setUint16(offset, sampleBits, true);
				offset += 2;
				writeString('data');
				offset += 4;
				data.setUint32(offset, dataLength, true);
				offset += 4;
				if (sampleBits === 8) {
					for (var i = 0; i < bytes.length; i++, offset++) {
						var s = Math.max(-1, Math.min(1, bytes[i]));
						var val = s < 0 ? s * 0x8000 : s * 0x7FFF;
						val = parseInt(255 / (65535 / (val + 32768)));
						data.setInt8(offset, val, true);
					}
				} else {
					for (var i = 0; i < bytes.length; i++, offset += 2) {
						var s = Math.max(-1, Math.min(1, bytes[i]));
						data.setInt16(offset, s < 0 ? s * 0x8000 : s * 0x7FFF, true);
					}
				}
				return new Blob([data], {
					type: 'audio_pcm/wav'
				});
			}
		};


		var sendData = function() {
			var reader = new FileReader();
			reader.onload = function(e) {
				var outbuffer = e.target.result;
				var arr = new Int8Array(outbuffer);
				if (arr.length > 0) {
					var tmparr = new Int8Array(1024);
					var j = 0;
					for (var i = 0; i < arr.byteLength; i++) {
						tmparr[j++] = arr[i];
						if (((i + 1) % 1024) == 0) {
							ws.send(tmparr);
							if (arr.byteLength - i - 1 >= 1024) {
								tmparr = new Int8Array(1024);
							} else {
								tmparr = new Int8Array(arr.byteLength - i - 1);
							}
							j = 0;
						}
						if ((i + 1 == arr.byteLength) && ((i + 1) % 1024) != 0) {
							ws.send(tmparr);
						}
					}
				}
			};
			reader.readAsArrayBuffer(audioData.encodePCM());
			audioData.clear();
		};

		this.start = function() {
			audioInput.connect(recorder);
			recorder.connect(context.destination);
		};

		this.stop = function() {
			recorder.disconnect();
		};

		this.getBlob = function() {
			return audioData.encodePCM();
		};

		this.clear = function() {
			audioData.clear();
		}

		recorder.onaudioprocess = function(e) {
			var inputBuffer = e.inputBuffer.getChannelData(0);
			audioData.input(inputBuffer);
			sendData();
		}
	};

	Recorder.throwError = function(message) {
		throw new function() {
			this.toString = function() {
				return message;
			}
		}
	};

	Recorder.canRecording = (navigator.getUserMedia != null);
	
	Recorder.useWebSocket = function(recorder, path, callback) {
		ws = new WebSocket(path);
		ws.binaryType = 'arraybuffer';
		ws.onopen = function() {
			if (ws.readyState == 1) {
				recorder.start();
			}
		};
		ws.onerror = function(err) {
			Recorder.throwError('websocket异常，异常信息：' + err);
		};
		ws.onmessage = function(res) {
			var data = res.data;
			if (data) {
				callback(JSON.parse(data));
			}
		}
	}
	
	Recorder.refreshSocket = function(rec, time, path, initback, callback) {
		setInterval(function() {
			ws.close();
			rec.stop();
			initback();
			setTimeout(function() {
				Recorder.useWebSocket(rec, path, callback);
			}, 500);
		}, time);
	}

	Recorder.get = function(config, initback, callback) {
		if (!navigator.getUserMedia) {
			Recorder.throwError('当前浏览器不支持录音功能。');
			return;
		}
		navigator.getUserMedia({
			audio: true
		}, function(stream) {
			var rec = new Recorder(stream, config);
			initback(rec);
			Recorder.useWebSocket(rec, config.wsPath, callback);
		}, function(error) {
			console.log(error)
			switch (error.code || error.name) {
				case 'PERMISSION_DENIED':
				case 'PermissionDeniedError':
					Recorder.throwError('用户拒绝提供信息。');
					break;
				case 'NOT_SUPPORTED_ERROR':
				case 'NotSupportedError':
					Recorder.throwError('浏览器不支持硬件设备。');
					break;
				case 'MANDATORY_UNSATISFIED_ERROR':
				case 'MandatoryUnsatisfiedError':
					Recorder.throwError('无法发现指定的硬件设备。');
					break;
				default:
					Recorder.throwError('无法打开麦克风。异常信息:' + (error.code || error.name));
					break;
			}
		});
	};

	window.Recorder = Recorder;
})(window);
