package com.realtime.voice.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.realtime.voice.util.Const;
import com.realtime.voice.util.Util;
import com.realtime.voice.util.Wlistener;

import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;
import okio.ByteString;

/**
 * 实时语音转写
 * 
 * @author park_
 *
 */
@Slf4j
@ServerEndpoint(value = "/ws/realtime/voice")
@Component
public class RealtimeVoiceWebSocket {

	/**
	 * 记录当前在线连接数
	 */
	private static AtomicInteger onlineCount = new AtomicInteger(0);

	private Map<String, WebSocket> webSocketMap = new HashMap<>();

	@OnOpen
	public void onOpen(Session session) {
		onlineCount.incrementAndGet();
		// 连接百度实时语音识别websocket
		run(session);
		log.info("有新连接加入：{}，当前在线人数为：{}", session.getId(), onlineCount.get());
	}

	@OnClose
	public void onClose(Session session) {
		onlineCount.decrementAndGet();
		close(session);
		log.info("有一连接关闭：{}，当前在线人数为：{}", session.getId(), onlineCount.get());
	}

	@OnMessage
	public void onMessage(InputStream inputStream, Session session) {
		// 一个帧 160ms的音频数据
		int bytesPerFrame = Util.BYTES_PER_FRAME;
		byte[] buffer = new byte[bytesPerFrame];
		int readSize;
		long nextFrameSendTime = System.currentTimeMillis();
		do {
			// 数据帧之间需要有间隔时间， 间隔时间为上一帧的音频长度
			Util.sleep(nextFrameSendTime - System.currentTimeMillis());
			try {
				readSize = inputStream.read(buffer);
			} catch (IOException | RuntimeException e) {
				log.info("inputstream is closed:" + e.getClass().getSimpleName() + ":" + e.getMessage());
				readSize = -2;
			}
			if (readSize > 0) {
				// readSize = -1 代表流结束
				ByteString bytesToSend = ByteString.of(buffer, 0, readSize);
				nextFrameSendTime = System.currentTimeMillis() + Util.bytesToTime(readSize);
				WebSocket websocket = webSocketMap.get(session.getId());
				websocket.send(bytesToSend);
			}
		} while (readSize >= 0);
	}

	@OnError
	public void onError(Session session, Throwable error) {
		log.error("发生错误");
		error.printStackTrace();
	}

	/**
	 * 连接百度实时语音识别
	 * 
	 * @param session
	 */
	public void run(Session session) {
		OkHttpClient client = new OkHttpClient.Builder().connectTimeout(2000, TimeUnit.MILLISECONDS).build();
		String url = Const.URI + "?sn=" + UUID.randomUUID().toString();
		log.info("runner begin: " + url);
		Request request = new Request.Builder().url(url).build();
		WebSocket websocket = client.newWebSocket(request, new Wlistener(session));
		// 预防多个连接
		webSocketMap.put(session.getId(), websocket);
		client.dispatcher().executorService().shutdown();
	}

	/**
	 * 关闭百度实时语音
	 * 
	 * @param session
	 */
	public void close(Session session) {
		JSONObject json = new JSONObject();
		json.put("type", "FINISH");
		log.info("send FINISH FRAME:" + json.toString());
		WebSocket websocket = webSocketMap.get(session.getId());
		websocket.send(json.toString());
		webSocketMap.remove(session.getId());
	}

}
